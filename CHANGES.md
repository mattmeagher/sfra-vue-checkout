
## 1.1.2  3/21/2019

- LRA-302	Silence ESlint warnings
- LRA-305	Checkout Card Validation bugfix
- LRA-304	Product Compare regression fix

## 1.1.1  2/26/2019

- LRA-240	Build Tools (Webpack Enhancements) ES6 Support
- LRA-220	Enhanced Navigation SFRA
- LRA-84	Wishlist search from login page
- LRA-254	Fixing Global styling of action butttons
- LRA-248	Fixed footer so column headers don't appear clickable
- LRA-231	Account Edit Profile Form Confirm Email Not Required If Email Unchanged
- LRA-203	Open Tracking consent modal on click in edit profile
- LRA-207	CVV validation added/fixed for saved cards
- LRA-241	Ability to clean cached files in build script
- LRA-265	Item numbers now will show in mobile view
- LRA-269	Fixed image issues with Product set variation images
- LRA-256	Safari - Double scroll in the mini cart
- LRA-255	Fixed to show logout link on mobile
- LRA-198	Manual product reccomendations now implemented
- LRA-268	Fix to show social icons on pdp mobile
- LRA-233	Account Registration password error message fix
- LRA-249	Footer blocked on PDP bug fixed
- LRA-263	Product tile swatch - ellipsis click and carry through swatch
- IEI-180	Fixed SARMS reporting in build script
- LRA-238	Link to PDP from cart line items
- LRA-272	Continue shopping link on cart

## 1.1.0  2/04/2019

- LRA-150	Responsive code fixes.
- LRA-158	removed default code value for data bundles so value can be inherited from dw.json
- LRA-119	Content asset in checkout
- LRA-117	Category browse breadcrumb path added
- LRA-168	Update data deploy script to use non-cert host for data import function
- LRA-165	verbose flag
- LRA-154	Styling issues
- LRA-155	Styling issues
- LRA-96	added toggle for product tile promo callout message
- LRA-170	Add default data bundle options for data_test and config_test
- LRA-141	SFRA oneline styleguide v2 update
- LRA-164	json validator
- LRA-178	Build script only runs tests on cartridges with tests
- LRA-144	Feature to swap product images via swatches on cat browse
- LRA-146	SEO attributes on store detail page
- LRA-147	Store details page
- LRA-148	Contact us page with Recaptcha
- LRA-134	My account header
- IEI-211	GTM cartridge conversion
- IEI-253	Cascading PageContext
- IEI-252	Default PageContext config
- LRA-186	Product tile path fix
- LRA-181	Build tools option to minimize css
- LRA-192	utils.js updates to enhance bild performance
- LRA-187	Moved homepage email signup to content asset
- LRA-145	Implement Zoom for PDP image, quickview, cart edit
- LRA-182	Improvements to CSS compilation time
- LRA-185	Improvements to JS compilation time
- LRA-162	Updating and annotating default site data
- LRA-190	Incorporating Webpack watcher in build tools
- LRA-189	Create category Grid template with content slot
- LRA-202	bitbucket pipeline yml added to run eslint
- LRA-193	bugfix for address book error message
- LRA-163	Webpack enhancements / Build script refactoring
- LRA-197	Salesforce 3.3.0 release tag merged in
- LRA-184	Resource message fix
- LRA-177	git pre-commit hooks using husky for linting
- LRA-207	Adding additional form validations to match SG functionality
- LRA-209	Webpack Enhancements with linting
- LRA-215	Manual Recommendations
- LRA ???	Fix Add My Account addressId check fix for adding and editing addresses
- LRA-201	Wishlist and Product compare added into our base offering
- LRA-216	Bonus Product Selection fixes
- LRA-236	Fixed multiple address editing issues in checkout
- LRA-217	Address Book Styling update
- LRA-218	My Account styling issues fixed
- LRA-222	collapsible arrows fixed
- LRA-224	Added missing slotBannerImage for categories
- LRA-227	Test helper for styling order confirmation page
- LRA-229	Better form field validation
- LRA-230	Adjusting maxlength for login fields
- LRA-239	Removed link to fullpage image in Zoom tool
- LRA-234	Phone number validation on address fields
- IEI-180	SARMS data collection

## 1.0.0  9/27/2018

- LRA-109 Build script overhaul to add lots of LyonsCG custom functionality, Webpack 4, new Bootstrap version etc.
- LRA-110 optional verbose build script log via setting in package.json
- LRA-115 New Bootstrap based style guide!
- LRA-123 Fixing SFRA source maps
- LRA-98 Added Mini Cart content slot
- LRA-125 Mobile Category navigation fix
- LRA-127 Fixing login bug from check order status page
- LRA-129 Fixing built in unit test command
- LRA-124 Bring our LyonsCG repo up to date with SFRA 3.2.0
- LRA-17 Updates so the Enhanced Navigation Cartridge is compatible with SFRA
- LRA-103 Support for cart abandonment integrations ( Marketing Cloud )
- LRA-194 New ANT task for automatic installation of npm modules on Jenkins
- LRA-18 Updating/fixing Carousel code to use Slick
- LRA-23 Fixing hard coded en language tag
- LRA-138 Fixing Bootstrap tooltips
- LRA-139 Option to disable commerce functionality sitewide
- LRA-132 Enabled new rule based metadata

